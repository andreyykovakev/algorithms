package collections;

import java.util.Iterator;

/**
 * Created by andreikovalev on 11/17/18.
 */

public abstract class AbstractCollection<T> {

    public abstract Iterator<T> iterator();

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator<T> iterator = iterator();
        while (iterator.hasNext()) {
            T value = iterator.next();
            if (value != null && iterator.hasNext()) {
                sb.append(value);
                if (iterator().hasNext()) {
                    sb.append(", ");
                }
            }
            if (!iterator.hasNext()) {
                sb.append(value);
                sb.append("]");
            }
        }
        return sb.toString();
    }
}
