package collections.arraylist;

import collections.AbstractList;
import collections.interfaces.List;

import java.util.Iterator;

/**
 * Created by andreikovalev on 10/24/18.
 */

public class ArrayList<T> extends AbstractList<T> implements List<T> {
    private Object[] array;
    private int size;

    ArrayList() {
        this.array = new Object[10];
        this.size = 0;
    }

    @Override
    public void add(T item) {
        int newElementPosition = size + 1;
        ensureCapacity(newElementPosition);
        array[size++] = item;
    }

    private void ensureCapacity(int newElementPosition) {
        if (array.length < newElementPosition) {
            Object[] newArray = new Object[(int) (array.length * 1.5)];
            System.arraycopy(array, 0, newArray, 0, array.length);
            array = newArray;
        }
    }

    @Override
    public int size() {
        return this.size;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T get(int index) {
        rangeCheck(index);
        return (T) array[index];
    }

    @Override
    public void remove(int index) {
        rangeCheck(index);
        int numMove = size - index - 1;
        System.arraycopy(array, index + 1, array, index, numMove);
        array[--size] = null;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    private void rangeCheck(int index) {
        if (index >= size)
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    @Override
    public Iterator<T> iterator() {
        return new CustomIterator();
    }

    @Override
    public void add(T item, int index) {
        rangeCheck(index);

        int newElementPosition = size + 1;
        ensureCapacity(newElementPosition);

        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = item;
        size++;
    }

    private class CustomIterator implements Iterator<T> {

        private int cursor;

        CustomIterator() {
            cursor = 0;
        }

        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        @SuppressWarnings("unchecked")
        @Override
        public T next() {
            Object[] array = ArrayList.this.array;
            Object nextItem = array[cursor];
            cursor++;
            return (T) nextItem;
        }
    }
}
