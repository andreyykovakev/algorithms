package collections.arraylist;

import collections.interfaces.List;

/**
 * Created by andreikovalev on 10/24/18.
 */

public class Demo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            list.add("string" + i);
        }

        list.add("INSERTED", 1);
        list.add("INSERTED", 3);
        list.add("INSERTED", 5);
        list.add("INSERTED", 7);

        for (int i = 0; i < 1000; i++) {
            list.add("" + i);
        }

        list.forEach(System.out::println);



    }
}
