package collections.interfaces;

public interface Collection<T> extends Iterable<T>{
    void add(T item);
    int size();
    T get(int index);
    void remove(int index);
}
