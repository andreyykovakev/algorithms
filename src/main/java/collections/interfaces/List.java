package collections.interfaces;

public interface List<T> extends Collection<T> {
    void add(T item, int index);
}
