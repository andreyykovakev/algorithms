package collections.linkedlist;

import collections.interfaces.List;

/**
 * Created by andreikovalev on 11/5/18.
 */

public class Demo {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        list.add("first");
        list.add("second");
        list.add("third");
        list.remove(1);

        for (String s : list) {
            System.out.println(s);
        }

        System.out.println(list);

    }
}
