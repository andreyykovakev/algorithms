package collections.linkedlist;

import collections.AbstractList;
import collections.interfaces.List;

import java.util.Iterator;

/**
 * Created by andreikovalev on 11/5/18.
 */

public class LinkedList<T> extends AbstractList<T> implements List<T>{
    private Node<T> head;
    private Node<T> first;
    private int size;

    public LinkedList() {
        this.head = null;
        this.first = null;
        this.size = 0;
    }

    @Override
    public void add(T item) {
        Node<T> newNode = new Node<>(null, item, null);

        if (head == null) {
            first = newNode;
            head = newNode;
        } else {
            head.next = newNode;
            newNode.prev = head;
            head = newNode;
        }
        size++;
    }

    public void add(T item, int index) {
        indexCheck(index);

        if (index == size) {
            add(item);
        }

        addBefore(node(index), item);

    }

    private void addBefore(Node<T> byIndex, T item) {

        Node<T> prev = byIndex.prev;
        Node<T> newNode = new Node<>(prev, item, byIndex);
        prev.next = newNode;
        byIndex.prev = newNode;

        size++;
    }

    private Node<T> node(int index) {
        Node<T> byIndex = first;
        for (int i = 0; i < index; i++) {
            byIndex = byIndex.next;
        }
        return byIndex;
    }

    private void indexCheck(int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        indexCheck(index);
        return node(index).item;
    }

    @Override
    public void remove(int index) {
        indexCheck(index);

        Node<T> current = node(index);
        final Node<T> prev = current.prev;
        final Node<T> next = current.next;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
        }

        if (next == null) {
            head = prev;
        } else {
            next.prev = prev;
        }

        current.prev = null;
        current.next = null;
        current.item = null;
        size--;
    }

    @Override
    public Iterator<T> iterator() {
        return new CustomIterator();
    }

    private class CustomIterator implements Iterator<T> {
        private Node<T> next;
        private Node<T> lastReturned;
        private int nextIndex;

        CustomIterator() {
            next = (size == 0) ? null : first;
            nextIndex = 0;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < size;
        }

        @Override
        public T next() {
            lastReturned = next;
            next = next.next;
            nextIndex++;

            return lastReturned.item;

        }
    }

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }
}
