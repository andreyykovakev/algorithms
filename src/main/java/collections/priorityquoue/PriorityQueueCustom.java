package collections.priorityquoue;

import java.util.ArrayList;
import java.util.List;

/**
 * @author andreikovalev on 4/24/19
 * @project algorithms
 */
public class PriorityQueueCustom<T extends Comparable<T>> {

    private int heapSize = 0;
    private List<T> heap;

    PriorityQueueCustom() {
        heap = new ArrayList<>();
    }

    public void add(T elem) {
        if (elem == null) throw new IllegalArgumentException();

        heap.add(elem);
        swim(heapSize);
        heapSize++;
    }

    private void swim(int childElemPosition) {

        // Grab the index of the next parentNodePosition node WRT to childElemPosition
        int parentNodePosition = (childElemPosition - 1) / 2;

        // Keep swimming while we have not reached the
        // root and while we're less than our parentNodePosition.
        while (childElemPosition > 0 && less(childElemPosition, parentNodePosition)) {
            // Exchange childElemPosition with the parentNodePosition
            swap(parentNodePosition, childElemPosition);
            childElemPosition = parentNodePosition;

            // Grab the index of the next parentNodePosition node WRT to childElemPosition
            parentNodePosition = (childElemPosition - 1) / 2;
        }
    }

    private void swap(int childElemPosition, int parentElemPosition) {
        T elem_i = heap.get(childElemPosition);
        T elem_j = heap.get(parentElemPosition);

        heap.set(childElemPosition, elem_j);
        heap.set(parentElemPosition, elem_i);
    }

    private boolean less(int childElemPosition, int parentElemPosition) {
        T childValue = heap.get(childElemPosition);
        T parentValue = heap.get(parentElemPosition);
        //return child less than parent
        return childValue.compareTo(parentValue) <= 0;
    }

    public boolean isEmpty() {
        return heapSize == 0;
    }

    public int size() {
        return heapSize;
    }

    public T poll() {
        return removeAt(0);
    }

    private T removeAt(int i) {
        if (isEmpty()) return null;

        heapSize--;
        T removed_data = heap.get(i);
        swap(i, heapSize);

        // Obliterate the value
        heap.set(heapSize, null);

        // Check if the last element was removed
        if (i == heapSize) return removed_data;
        T elem = heap.get(i);

        // Try sinking element
        sink(i);

        // If sinking did not work try swimming
        if (heap.get(i).equals(elem))
            swim(i);
        return removed_data;
    }

    private void sink(int k) {
        while ( true ) {
            int left  = 2 * k + 1; // Left  node
            int right = 2 * k + 2; // Right node
            int smallest = left;   // Assume left is the smallest node of the two children

            // Find which is smaller left or right
            // If right is smaller set smallest to be right
            if (right < heapSize && less(right, left))
                smallest = right;

            // Stop if we're outside the bounds of the tree
            // or stop early if we cannot sink k anymore
            if (left >= heapSize || less(k, smallest)) break;

            // Move down the tree following the smallest node
            swap(smallest, k);
            k = smallest;
        }
    }

    public T peek(){
        if(isEmpty()) return null;
        return (T) heap.get(0);
    }
}
