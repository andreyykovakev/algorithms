package dijkstra;

import javax.script.ScriptEngineManager;
import java.io.PrintStream;

/**
 * @author andreikovalev on 3/14/19
 * @project algorithms
 */
public class Calculator {
    private static void calc(String expression) {
        try {
            System.out.println(new ScriptEngineManager().getEngineByName("JavaScript").eval(expression));
        } catch (Exception ex) {
            try (PrintStream stream = (System.out.append("Nan"))) {}
        }
    }

    public static void main(String[] args) {
        calc("+5 + -12");
        calc("+5 * -12");
        calc("+5 - -12");
        calc("+5 / -12");
    }
}
