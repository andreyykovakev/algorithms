package dijkstra;

import java.util.Stack;

/**
 * @author andreikovalev on 3/14/19
 * @project algorithms
 */
public class Evaluate {
    public static void main(String[] args) {

        String input = "((1+3)*(3+2))";

        Stack<String> operands = new Stack<>();
        Stack<Double> values = new Stack<>();

        for (Character c : input.toCharArray()) {
            String s = String.valueOf(c);
            if(s.equals("("));

            else if(s.equals("+")){
                operands.push(s);
            }

            else if(s.equals("-")){
                operands.push(s);
            }

            else if(s.equals("/")){
                operands.push(s);
            }

            else if(s.equals("*")){
                operands.push(s);
            }

            else if(s.equals(")")){
                String op = operands.pop();
                Double val = values.pop();

                if(op.equals("+")){
                    val = values.pop() + val;
                }

                else if(op.equals("-")){
                    val = values.pop() - val;
                }

                else if(op.equals("/")){
                    val = values.pop() / val;
                }

                else if(op.equals("*")){
                    val = values.pop() * val;
                }

                values.push(val);
            }

            else values.push(Double.parseDouble(s));
        }
        System.out.println(values.pop());
    }
}
