package helpers;

import java.util.List;

/**
 * @author andreikovalev on 3/20/19
 * @project algorithms
 */
public class Helpers {

    public static <IN> double calculateAverage(List<IN> ins, MapperToDouble<IN> mapper) {

        double sum = 0.0;

        for (IN in : ins) {
            double out = mapper.map(in);
            sum += out;
        }

        return sum / ins.size();
    }

    public static <IN, OUT> OUT calculate(List<IN> ins, MapperToWhatever<IN, OUT> mapper, CustomAccumulator<OUT> accumulator, Initializer<OUT> initializer) {

        OUT sum = initializer.init();

        for (IN in : ins) {
            OUT out = mapper.map(in);
            sum = accumulator.sum(out, sum);
        }

        return sum;
    }

    public interface MapperToDouble<IN> {
        double map(IN in);
    }

    public interface MapperToWhatever<IN, OUT> {
        OUT map(IN in);
    }

    public interface CustomAccumulator<IN> {
        IN sum(IN in1, IN in2);
    }

    public interface Initializer<V> {
        V init();
    }

}
