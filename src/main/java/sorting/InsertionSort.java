package sorting;

/**
 * @author andreikovalev on 3/21/19
 * @project algorithms
 */
public class InsertionSort {

    public static int[] sort(int[] list) {
        int i, j, key, temp;

        for (i = 1; i < list.length; i++) {

            key = list[i];
            j = i - 1;

            while (j >= 0 && key < list[j]) {
                temp = list[j];
                list[j] = list[j + 1];
                list[j + 1] = temp;
                j--;
            }
        }
        return list;
    }

    public static void main(String[] args) {

        int[] nums = new int[]{1, 4, 3, 4};

        InsertionSort.sort(nums);

        for (int num : nums) {
            System.out.println(num);
        }
    }
}
