package sorting;

/**
 * @author andreikovalev on 3/21/19
 * @project algorithms
 */
public class SelectionSort {

    public static void sort(int[] numbers) {

        int temp, min;

        for (int i = 0; i < numbers.length - 1; i++) {

            min = i;

            for (int j = i + 1; j < numbers.length; j++) {

                if (numbers[j] < numbers[i]) {
                    min = j;
                }
            }

            temp = numbers[min];
            numbers[min] = numbers[i];
            numbers[i] = temp;
        }
    }

    public static <T extends Comparable<T>> void sort(T[] items) {

        int min;
        T temp;

        for (int i = 0; i < items.length - 1; i++) {

            min = i;

            for (int j = i + 1; j < items.length; j++) {

                if (items[j].compareTo(items[i]) < 0) {
                    min = j;
                }
            }

            temp = items[min];
            items[min] = items[i];
            items[i] = temp;
        }
    }

    public static void main(String[] args) {

        int[] nums = new int[]{1, 4, 3, 4};

        SelectionSort.sort(nums);

        for (int num : nums) {
            System.out.println(num);
        }
    }
}
