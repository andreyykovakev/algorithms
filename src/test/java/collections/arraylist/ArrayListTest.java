package collections.arraylist;

import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

public class ArrayListTest {
    private ArrayList<String> sut = new ArrayList<>();
    private static final String[] TEST_VALUES = {"test1", "test2", "test3"};

    @Test
    public void addOneValue() {
        sut.add(TEST_VALUES[0]);

        String expected = TEST_VALUES[0];
        String actual = sut.get(0);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sizeAfterAddingAndRemovingValues() {
        sut.add(TEST_VALUES[0]);
        sut.add(TEST_VALUES[1]);
        sut.remove(0);

        Assert.assertEquals(1, sut.size());
    }

    @Test
    public void addAndGetManyValues() {
        for (String testVal : TEST_VALUES) {
            sut.add(testVal);
        }

        for (int i = 0; i < sut.size(); i++) {
            Assert.assertEquals(TEST_VALUES[i], sut.get(i));
        }
    }

    @Test
    public void remove() {
        for (String testVal : TEST_VALUES) {
            sut.add(testVal);
        }

        sut.remove(0);
        sut.remove(1);

        Assert.assertEquals("test2",sut.get(0));
    }

    @Test
    public void iterator() {
        sut.add(TEST_VALUES[0]);
        sut.add(TEST_VALUES[1]);

        Iterator iter = sut.iterator();

        Assert.assertTrue(iter.hasNext());
        Assert.assertEquals("test1", iter.next());
        Assert.assertEquals("test2", iter.next());
    }
}