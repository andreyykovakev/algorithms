package collections.priorityquoue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author andreikovalev on 4/26/19
 * @project algorithms
 */
public class PriorityQueueCustomTest {
    private PriorityQueueCustom<Integer> queue;

    @Before
    public void init() {
        queue = new PriorityQueueCustom<>();
    }

    @Test
    public void addIncreasesSize() {
        queue.add(1);
        queue.add(12);

        Assert.assertEquals(2, queue.size());
    }

    @Test
    public void addDoesInsertionToARightPosition() {
        queue.add(1);
        queue.add(12);

        Assert.assertEquals(1, (long) queue.poll());
        Assert.assertEquals(12, (long) queue.poll());
    }

    @Test
    public void isEmpty() {
        queue.add(1);

        Assert.assertEquals(1, queue.size());
    }

    @Test
    public void size() {
        queue.add(1);
        queue.add(12);

        Assert.assertTrue(!queue.isEmpty());
    }

    @Test
    public void poll() {
        queue.add(1);
        queue.add(12);

        Assert.assertEquals(1, (long) queue.poll());
        Assert.assertEquals(12, (long) queue.poll());
    }

    @Test
    public void peek() {
        queue.add(1);
        queue.add(12);

        Assert.assertEquals(1, (long) queue.peek());
        Assert.assertEquals(1, (long) queue.peek());
    }
}